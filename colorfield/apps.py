from django.apps import AppConfig


class PostgresqlRgbColorfieldConfig(AppConfig):
    name = 'colorfield'
